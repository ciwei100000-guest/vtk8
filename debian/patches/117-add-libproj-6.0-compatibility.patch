From: https://gitlab.kitware.com/vtk/vtk/-/merge_requests/5582
      https://gitlab.kitware.com/vtk/vtk/-/merge_requests/7731
Date: Thu, 17 Feb 2022 06:28:50 -0800
Subject: [PATCH] Add libproj 6.0 compatibility

diff --git a/CMake/FindLibPROJ.cmake b/CMake/FindLibPROJ.cmake
index 2d8301dc..b909bebb 100644
--- a/CMake/FindLibPROJ.cmake
+++ b/CMake/FindLibPROJ.cmake
@@ -30,7 +30,7 @@ if ( NOT LibPROJ_INCLUDE_DIR OR NOT LibPROJ_LIBRARIES OR NOT LibPROJ_FOUND )
   )
 
   find_path( LibPROJ_INCLUDE_DIR
-    NAMES proj_api.h
+    NAMES proj.h
     HINTS
       ${_LibPROJ_DIR}
       ${_LibPROJ_DIR}/include
diff --git a/Geovis/Core/vtkGeoProjection.cxx b/Geovis/Core/vtkGeoProjection.cxx
index f3a8852d..ca7fe1d4 100644
--- a/Geovis/Core/vtkGeoProjection.cxx
+++ b/Geovis/Core/vtkGeoProjection.cxx
@@ -72,6 +72,7 @@ public:
   }
 
   std::map< std::string, std::string > OptionalParameters;
+  PJ_PROJ_INFO ProjInfo;
 };
 
 //-----------------------------------------------------------------------------
@@ -80,7 +81,7 @@ int vtkGeoProjection::GetNumberOfProjections()
   if ( vtkGeoProjectionNumProj < 0 )
   {
     vtkGeoProjectionNumProj = 0;
-    for ( const PJ_LIST* pj = pj_get_list_ref(); pj && pj->id; ++ pj )
+    for ( const PJ_LIST* pj = proj_list_operations(); pj && pj->id; ++ pj )
       ++ vtkGeoProjectionNumProj;
   }
   return vtkGeoProjectionNumProj;
@@ -91,7 +92,7 @@ const char* vtkGeoProjection::GetProjectionName( int projection )
   if ( projection < 0 || projection >= vtkGeoProjection::GetNumberOfProjections() )
     return nullptr;
 
-  return pj_get_list_ref()[projection].id;
+  return proj_list_operations()[projection].id;
 }
 //-----------------------------------------------------------------------------
 const char* vtkGeoProjection::GetProjectionDescription( int projection )
@@ -99,7 +100,7 @@ const char* vtkGeoProjection::GetProjectionDescription( int projection )
   if ( projection < 0 || projection >= vtkGeoProjection::GetNumberOfProjections() )
     return nullptr;
 
-  return pj_get_list_ref()[projection].descr[0];
+  return proj_list_operations()[projection].descr[0];
 }
 //-----------------------------------------------------------------------------
 vtkGeoProjection::vtkGeoProjection()
@@ -120,7 +121,7 @@ vtkGeoProjection::~vtkGeoProjection()
   this->SetPROJ4String( nullptr );
   if ( this->Projection )
   {
-    pj_free( this->Projection );
+    proj_destroy( this->Projection );
   }
   delete this->Internals;
   this->Internals = nullptr;
@@ -144,7 +145,7 @@ void vtkGeoProjection::PrintSelf( ostream& os, vtkIndent indent )
 int vtkGeoProjection::GetIndex()
 {
   int i = 0;
-  for ( const PJ_LIST* proj = pj_get_list_ref(); proj && proj->id; ++ proj, ++ i )
+  for ( const PJ_LIST* proj = proj_list_operations(); proj && proj->id; ++ proj, ++ i )
   {
     if ( ! strcmp( proj->id, this->Name ) )
     {
@@ -161,7 +162,7 @@ const char* vtkGeoProjection::GetDescription()
   {
     return nullptr;
   }
-  return this->Projection->descr;
+  return this->Internals->ProjInfo.description;
 }
 //-----------------------------------------------------------------------------
 projPJ vtkGeoProjection::GetProjection()
@@ -180,13 +181,13 @@ int vtkGeoProjection::UpdateProjection()
 
   if ( this->Projection )
   {
-    pj_free( this->Projection );
+    proj_destroy( this->Projection );
     this->Projection = nullptr;
   }
 
   if ( this->PROJ4String && strlen( this->PROJ4String ) )
   {
-    this->Projection = pj_init_plus( this->PROJ4String );
+    this->Projection = proj_create(PJ_DEFAULT_CTX, this->PROJ4String);
   }
   else
   {
@@ -226,12 +227,13 @@ int vtkGeoProjection::UpdateProjection()
       pjArgs[3+i] = stringHolder[i].c_str();
     }
 
-    this->Projection = pj_init( argSize, const_cast<char**>( pjArgs ) );
+    this->Projection = proj_create_argv(PJ_DEFAULT_CTX, argSize, const_cast<char**>(pjArgs));
     delete[] pjArgs;
   }
   this->ProjectionMTime = this->GetMTime();
   if ( this->Projection )
   {
+    this->Internals->ProjInfo = proj_pj_info(this->Projection);
     return 0;
   }
   return 1;
diff --git a/Geovis/Core/vtkGeoTransform.cxx b/Geovis/Core/vtkGeoTransform.cxx
index aeeabc10..9deb93a5 100644
--- a/Geovis/Core/vtkGeoTransform.cxx
+++ b/Geovis/Core/vtkGeoTransform.cxx
@@ -159,17 +159,18 @@ void vtkGeoTransform::InternalTransformPoints( double* x, vtkIdType numPts, int
   projPJ src = this->SourceProjection ? this->SourceProjection->GetProjection() : nullptr;
   projPJ dst = this->DestinationProjection ? this->DestinationProjection->GetProjection() : nullptr;
   int delta = stride - 2;
-  projLP lp;
-  projXY xy;
+  PJ_COORD c, c_out;
   if ( src )
   {
     // Convert from src system to lat/long using inverse of src transform
     double* coord = x;
     for ( vtkIdType i = 0; i < numPts; ++ i )
     {
-      xy.u = coord[0]; xy.v = coord[1];
-      lp = pj_inv( xy, src );
-      coord[0] = lp.u; coord[1] = lp.v;
+      c.xy.x = coord[0];
+      c.xy.y = coord[1];
+      c_out = proj_trans(src, PJ_INV, c);
+      coord[0] = c_out.lp.lam;
+      coord[1] = c_out.lp.phi;
       coord += stride;
     }
   }
@@ -191,9 +192,11 @@ void vtkGeoTransform::InternalTransformPoints( double* x, vtkIdType numPts, int
     double* coord = x;
     for ( vtkIdType i = 0; i < numPts; ++ i )
     {
-      lp.u = coord[0]; lp.v = coord[1];
-      xy = pj_fwd( lp, dst );
-      coord[0] = xy.u; coord[1] = xy.v;
+      c.lp.lam = coord[0];
+      c.lp.phi = coord[1];
+      c_out = proj_trans(src, PJ_FWD, c);
+      coord[0] = c_out.xy.x;
+      coord[1] = c_out.xy.y;
       coord += stride;
     }
   }
diff --git a/ThirdParty/libproj/vtk_libproj.h.in b/ThirdParty/libproj/vtk_libproj.h.in
index cd9edc3a..08f2e3c4 100644
--- a/ThirdParty/libproj/vtk_libproj.h.in
+++ b/ThirdParty/libproj/vtk_libproj.h.in
@@ -18,8 +18,7 @@
 /* Use the libproj library configured for VTK.  */
 #cmakedefine VTK_USE_SYSTEM_LIBPROJ
 #ifdef VTK_USE_SYSTEM_LIBPROJ
-# include <projects.h>
-# include <proj_api.h>
+# include <proj.h>
 # include <geodesic.h>
 #else
 # include <vtklibproj/src/projects.h>
